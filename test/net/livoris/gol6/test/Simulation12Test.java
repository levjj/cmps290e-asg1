package net.livoris.gol6.test;

import java.util.Arrays;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import net.livoris.gol6.Grid;
import net.livoris.gol6.Simulation;

public class Simulation12Test {
	
	private Grid grid;
	private Simulation sim;

	@Before
	public void setUp() {
		grid = new Grid(5);
		sim = new Simulation.R12();
	}
	
	@Test
	public void test() {
		grid = grid.load(Arrays.asList(new Boolean[]{
			false,        false,        false,       false,        false,
		   		  false,        false,        true,        true,          false,
		   	false,        false,        true,        true,        false,
	   		      false,        true,        false,        true,          false,
	   		false,        false,        false,       false,        false
		}));
		grid = grid.map(sim);
		Assert.assertArrayEquals(new Boolean[]{
			false,        false,        false,       true,        false,
		   		  false,        true,         true,        true,          false,
		   	false,        true,         false,        false,       false,
	   		      false,        false,        false,        false,        false,
	   		false,        false,        false,       false,       false
		}, grid.asBoolArray());
	}
}
