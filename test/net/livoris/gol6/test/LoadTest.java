package net.livoris.gol6.test;

import java.util.Arrays;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import net.livoris.gol6.Grid;

public class LoadTest {
	
	private Grid grid;
	
	@Before
	public void setUp() {
		grid = new Grid(3);
	}
	
	@Test
	public void boolArray() {
		Boolean[] d = new Boolean[]{
			true,        false,        false,
		   		  true,        false,        false,
			true,       false,        true
		};
		Assert.assertArrayEquals(d, grid.load(Arrays.asList(d)).asBoolArray());
	}
	
	@Test
	public void strings() {
		Boolean[] d = new Boolean[]{
			true,        false,        false,
		   		  true,        false,        false,
			true,       false,        true
		};
		List<String> strings = Arrays.asList(new String[] {
			"X..",
			"X..",
			"X.X"
		});
		Assert.assertArrayEquals(d, grid.load(strings).asBoolArray());
	}

	@Test
	public void stringsExtra() {
		Boolean[] d = new Boolean[]{
			true,        false,        false,
		   		  true,        false,        false,
			true,       false,        true
		};
		List<String> strings = Arrays.asList(new String[] {
			"X..",
			"X..XXXXX",
			"X.X",
			"XXX.X"
		});
		Assert.assertArrayEquals(d, grid.load(strings).asBoolArray());
	}

	@Test(expected=RuntimeException.class)
	public void missingLines() {
		grid.load(Arrays.asList(new String[] {
			"X..",
			"X.."
		}));
	}

	@Test(expected=RuntimeException.class)
	public void missingChars() {
		grid.load(Arrays.asList(new String[] {
			"X..",
			"X.",
			"X.X"
		}));
	}

	@Test(expected=RuntimeException.class)
	public void wrongChars() {
		grid.load(Arrays.asList(new String[] {
			"X..",
			"ABC",
			"X.X"
		}));
	}
}
