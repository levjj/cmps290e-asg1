package net.livoris.gol6.test;

import java.util.Arrays;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import net.livoris.gol6.Grid;
import net.livoris.gol6.Simulation;

public class SimulationTest {
	
	private Grid grid;
	private Simulation sim;

	@Before
	public void setUp() {
		grid = new Grid(3);
		sim = new Simulation.R6();
	}
	
	@Test
	public void starvation() {
		grid = grid.load(Arrays.asList(new Boolean[]{
			true,        false,        false,
		   		  false,        false,        false,
			false,       false,        false
		}));
		grid = grid.map(sim);
		Assert.assertArrayEquals(new Boolean[]{
			false,        false,        false,
		   		  false,        false,        false,
			false,       false,        false
		}, grid.asBoolArray());
	}
	
	@Test
	public void overcrowded() {
		grid = grid.load(Arrays.asList(new Boolean[]{
			true,        true,        false,
		   		  true,        true,        false,
			true,        true,        false
		}));
		grid = grid.map(sim);
		Assert.assertArrayEquals(new Boolean[]{
			true,        true,        false,
		   		  false,        true,        false,
			true,       true,         false
		}, grid.asBoolArray());
	}
	
	@Test
	public void reproduction() {
		grid = grid.load(Arrays.asList(new Boolean[]{
			true,        true,        true,
		   		  true,        false,        false,
			true,        false,        false
		}));
		grid = grid.map(sim);
		Assert.assertArrayEquals(new Boolean[]{
			true,        true,        false,
		   		  true,        true,        false,
			false,       false,         false
		}, grid.asBoolArray());
	}
}
