package net.livoris.gol6.test;
import static org.junit.Assert.*;

import org.junit.Test;

import net.livoris.gol6.Grid;

public class GridGenerationTest {
	
	private Grid grid;

	private int numberOfLiveCells(Grid g) {
		return g.reduce((alive, pos, prev) -> alive ? prev + 1 : prev, 0);
	}

	@Test
	public void zeroInit() {
		grid = new Grid(100).random(0.0);
		assertEquals(0, numberOfLiveCells(grid));
	}
	
	@Test
	public void oneInit() {
		grid = new Grid(100).random(1.0);
		assertEquals(100 * 100, numberOfLiveCells(grid));
	}
}
