package net.livoris.gol6.test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import net.livoris.gol6.Grid;

public class GridNeighborsTest {

	private Grid grid;

	@Before
	public void setup() {
		grid = new Grid(3);
	}

	@Test
	public void topLeft() {
		assertEquals(2, grid.neighbors(new Grid.Pos(0, 0)).count());
	}

	@Test
	public void topCentral() {
		assertEquals(4, grid.neighbors(new Grid.Pos(1, 0)).count());
	}

	@Test
	public void topRight() {
		assertEquals(3, grid.neighbors(new Grid.Pos(2, 0)).count());
	}

	@Test
	public void centralLeft() {
		assertEquals(5, grid.neighbors(new Grid.Pos(0, 1)).count());
	}
	
	@Test
	public void central() {
		assertEquals(6, grid.neighbors(new Grid.Pos(1, 1)).count());
	}
	
	@Test
	public void centralRight() {
		assertEquals(3, grid.neighbors(new Grid.Pos(2, 1)).count());
	}
	
	@Test
	public void bottomLeft() {
		assertEquals(2, grid.neighbors(new Grid.Pos(0, 2)).count());
	}

	@Test
	public void bottomCentral() {
		assertEquals(4, grid.neighbors(new Grid.Pos(1, 2)).count());
	}

	@Test
	public void bottomRight() {
		assertEquals(3, grid.neighbors(new Grid.Pos(2, 2)).count());
	}
	
	@Test
	public void topLeft12() {
		assertEquals(2, grid.secondNeighbors(new Grid.Pos(0, 0)).count());
	}

	@Test
	public void topCentral12() {
		assertEquals(2, grid.secondNeighbors(new Grid.Pos(1, 0)).count());
	}

	@Test
	public void leftCentral12() {
		assertEquals(2, grid.secondNeighbors(new Grid.Pos(0, 1)).count());
	}

	@Test
	public void central12() {
		assertEquals(2, grid.secondNeighbors(new Grid.Pos(1, 1)).count());
	}
}
