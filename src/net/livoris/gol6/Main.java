package net.livoris.gol6;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

import org.kohsuke.args4j.CmdLineException;
import org.kohsuke.args4j.CmdLineParser;
import org.kohsuke.args4j.Option;

public class Main {

	@Option(name = "-12", usage = "use the 12 neighbor rules (default is the 6 neighbor rules)")
	private boolean rule12;

	@Option(name = "-size", usage = "specify the size of the grid to be n X n", metaVar = "N")
	private int size = 100;

	@Option(name = "-f", usage = "read in the initial configuration from the specified file", metaVar = "FILENAME")
	private File file;

	@Option(name = "-g", usage = "specifiy the number of generations to simulate", metaVar = "N")
	private int generations = 10;

	@Option(name = "-p", usage = "specify that every nth generation should be printed (for plain text output only)", metaVar = "N")
	private int printn = 1;

	@Option(name = "-i", usage = "specify the probabilty of a cell being alive in the initial configuration if no file is provided", metaVar = "P")
	private double prob = 0.5;

	@Option(name = "-u", usage = "show a graphical user interface")
	private boolean gui = false;

	@Option(name = "-h", aliases = "-help", usage = "show this message", help = true)
	private boolean help;

	public void run() throws IOException {
		Simulation sim = rule12 ? new Simulation.R12() : new Simulation.R6();
		Output out = gui ? new GraphicalOutput() : new ConsoleOutput(printn);
		Grid grid = new Grid(size);
		if (file == null) {
			grid = grid.random(prob);
		} else {
	    	grid = grid.load(Files.readAllLines(file.toPath()));
		}
		out.show(grid);
		for (int i = 0; i < generations; i++) {
			grid = grid.map(sim);
			out.show(grid);
		}
	}

	public static void main(String[] args) {
		Main main = new Main();
		CmdLineParser parser = new CmdLineParser(main);
		try {
			parser.parseArgument(args);
			if (main.help) {
				parser.printUsage(System.out);
			} else {
				main.run();
			}
		} catch (CmdLineException e) {
			System.err.println(e.getMessage());
			System.err.println("java net.livoris.gol6.Main [options...]");
			parser.printUsage(System.err);
			System.err.println();
		} catch (Exception e) {
			System.err.println(e.getMessage());
		}
	}
}