package net.livoris.gol6;

public class ConsoleOutput implements Output {
	
	private int printn = 1;
	private int step;
	
	public ConsoleOutput(int printn) {
		this.printn = printn;
		this.step = 0;
	}
	
	public void show(Grid grid) {
		if (++step == printn) {
			step = 0;
			print(grid);
		}
	}
	
	private void print(Grid grid) {
		System.out.println();
		grid.reduce((alive,  pos, prev) -> {
			if (pos.x == 0) System.out.println();
			if (pos.x != 0 || pos.y % 2 == 1) {
				System.out.print(' ');
			}
			System.out.print(alive ? 'X' : '.');
			return prev;
		}, null);
	}
}
