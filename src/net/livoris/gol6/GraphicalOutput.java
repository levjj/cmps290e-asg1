package net.livoris.gol6;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Graphics;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JSlider;

public class GraphicalOutput implements Output {
	
	private HexDisplay display;
	private JSlider slider;
	private JButton button;
	private List<Grid> grids = new ArrayList<>();
	private boolean playing = false;

	public void show(Grid grid) {
		grids.add(grid);
		if (grids.size() == 1) {
			initializeUI(grid);
			startStepping();
		}
		slider.setMinimum(0);
		slider.setMaximum(grids.size() - 1);
	}
	
	public void initializeUI(Grid grid) {
		JFrame frame = new JFrame("Game Of Live (Hexagon)");
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

		JPanel contentPane = new JPanel();
		contentPane.setLayout(new BoxLayout(contentPane, BoxLayout.Y_AXIS));
		display = new HexDisplay(grid);
		contentPane.add(display);
		
		JPanel controlPane = new JPanel();
		controlPane.setLayout(new FlowLayout(FlowLayout.CENTER));
		
		slider = new JSlider();
		slider.addChangeListener(c -> {
			display.showGrid(grids.get(slider.getValue()));
		});
		controlPane.add(slider);
		
		button = new JButton("  Play  ");
		button.addActionListener(ae -> {
			playing = !playing;
			button.setText(playing ? "Pause" : "  Play  ");
		});
		controlPane.add(button);
		contentPane.add(controlPane);

		frame.setContentPane(contentPane);
		frame.pack();
		frame.setLocationByPlatform(true);
		frame.setVisible(true);
	}

	private void startStepping() {
		ScheduledExecutorService sched = Executors.newSingleThreadScheduledExecutor(r -> {
			Thread thread = new Thread(r);
	        thread.setDaemon(true);
	        return thread;
		});
		sched.scheduleAtFixedRate(() -> {
			if (playing) {
				if (slider.getValue() == slider.getMaximum()) {
					playing = false;
					button.setText("  Play  ");
				} else {
					slider.setValue(slider.getValue() + 1);
				}
			}
		}, 100, 100, TimeUnit.MILLISECONDS);
	}

	private static class HexDisplay extends JPanel {

		private static final long serialVersionUID = 1L;
		
		private static final int xScale = 17;
		private static final int yScale = 16;
		private static final int margin = 8;
		
		private Grid grid;
		private Font font;

		public HexDisplay(Grid grid) {
			super();
			setOpaque(true);
			this.grid = grid;
			this.font = getFont().deriveFont(22.0f);
		}
		
		public void showGrid(Grid g) {
			this.grid = g;
			setSize(g.getSize() * xScale + 2 * margin,
					g.getSize() * yScale + 2 * margin);
			repaint();
		}

		@Override
		public Dimension getPreferredSize() {
			return new Dimension(grid.getSize() * xScale + 2 * margin,
					             grid.getSize() * yScale + 2 * margin);
		}
		
		@Override
		protected void paintComponent(Graphics g) {
			super.paintComponent(g);
			g.setFont(font);
			grid.reduce((alive, pos, prev) -> {
				g.setColor(alive ? Color.BLACK : Color.WHITE); 
				int xPos = pos.x * xScale + ((pos.y % 2) * xScale / 2) + margin;
				int yPos = (pos.y + 1) * yScale + margin;
				g.drawString("\u2B22", xPos, yPos);
				return prev;
			}, null);
		}
	}
}
