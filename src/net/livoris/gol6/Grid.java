package net.livoris.gol6;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Random;
import java.util.function.Predicate;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Grid {

	public static class Pos {
		public final int x, y;
		public Pos(int x, int y) {
			this.x = x;
			this.y = y;
		}
	}
	
	@FunctionalInterface
	public interface Reducer<R> {
		public R apply(boolean alive, Pos pos, R prev);
	}

	@FunctionalInterface
	public interface Mapper {
		public boolean apply(boolean alive, Pos pos, Grid grid);
	}

	private final boolean[][] data;

	public Grid(int size) {
		data = new boolean[size][size];
	}

	public boolean isCellAlive(Pos p) {
		return data[p.x][p.y];
	}

	public int getSize() {
		return data.length;
	}

	public <R> R reduce(Reducer<R> r, R initial) {
		R result = initial;
		for (int y = 0; y < data.length; y++) {
			for (int x = 0; x < data[y].length; x++) {
				result = r.apply(data[x][y], new Pos(x, y), result);
			}
		}
		return result;
	}
	
	public Grid map(Mapper m) {
		return reduce((alive, pos, grid) -> {
			grid.data[pos.x][pos.y] = m.apply(alive, pos, this);
			return grid;
		}, new Grid(this.data.length));
	}

	public Grid random(double prob) {
		Random r = new Random();
		return map((a, pos, grid) -> r.nextDouble() < prob);
	}
	
	public Grid load(List<Boolean> lst) {
		return map((a, pos, grid) -> {
			if (lst.size() < pos.x + pos.y * data.length) {
				throw new RuntimeException("Input configuration has wrong size!");
			}
			return lst.get(pos.x + pos.y * data.length);
		});
	}
	
	private static final Predicate<String> LINE_FORMAT = Pattern.compile("^[X\\.]+$").asPredicate();
	
	public Grid load(Collection<String> lines) {
    	if (lines.size() < data.length||
    		lines.stream().anyMatch(s -> s.length() < data.length || !LINE_FORMAT.test(s))) {
    		throw new RuntimeException("Input configuration has wrong size/format!");
    	}
        return load(lines.stream()
        	.map(s -> s.substring(0, data.length)) // truncate lines
        	.flatMap(s -> s.chars().mapToObj(i -> (char)i == 'X')) // X = alive
        	.collect(Collectors.toList()));
	}
	
	public Stream<Pos> neighbors(Pos p) {
		return Arrays.stream(new Pos[]{
			// left
			new Pos(p.x - 1, p.y),
			// top
			new Pos(p.x, p.y - 1),
			// right
			new Pos(p.x + 1, p.y),
			// bottom
			new Pos(p.x, p.y + 1),
			// top left or top right
			p.y % 2 == 0 ? new Pos(p.x - 1, p.y - 1) : new Pos(p.x + 1, p.y - 1),
			// bottom left or bottom right
			p.y % 2 == 0 ? new Pos(p.x - 1, p.y + 1) : new Pos(p.x + 1, p.y + 1)
		}).filter(pos -> pos.x >= 0 && pos.x < data.length && pos.y >= 0 && pos.y < data.length);
	}
	
	public Stream<Pos> secondNeighbors(Pos p) {
		return Arrays.stream(new Pos[]{
			// top
			new Pos(p.x, p.y - 2),
			// bottom
			new Pos(p.x, p.y + 2),
			// top left
			p.y % 2 == 0 ? new Pos(p.x - 2, p.y - 1) : new Pos(p.x - 1, p.y - 1),
			// bottom left
			p.y % 2 == 0 ? new Pos(p.x - 2, p.y + 1) : new Pos(p.x - 1, p.y + 1),
			// top right
			p.y % 2 == 0 ? new Pos(p.x + 1, p.y - 1) : new Pos(p.x + 2, p.y - 1),
			// bottom right
			p.y % 2 == 0 ? new Pos(p.x + 1, p.y + 1) : new Pos(p.x + 2, p.y + 1)
		}).filter(pos -> pos.x >= 0 && pos.x < data.length && pos.y >= 0 && pos.y < data.length);
	}
	
	public Boolean[] asBoolArray() {
		return reduce((alive,  pos, prev) -> {
			prev[pos.x + pos.y * data.length] = alive;
			return prev;
		}, new Boolean[data.length * data.length]);
	}
}