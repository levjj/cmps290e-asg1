package net.livoris.gol6;

import java.util.stream.Stream;

public abstract class Simulation implements Grid.Mapper {
	
	public static class R6 extends Simulation {
		
		@Override
		public boolean apply(boolean alive, Grid.Pos pos, Grid grid) {
			int liveNeighbors = grid
				.neighbors(pos)
				.map(p -> grid.isCellAlive(p) ? 1 : 0)
				.reduce(0, (a,b) -> a + b);
			
			if (alive) {
			    // Any live cell with fewer than two live neighbors dies, as if caused by under-population.
				if (liveNeighbors < 2) return false;
				// Any live cell with two or three live neighbors lives on to the next generation.
				if (liveNeighbors == 2 || liveNeighbors == 3) return true;
			    // Any live cell with more than three live neighbors dies, as if by overcrowding.
				return false;
			} else {
				// Any dead cell with exactly three live neighbors becomes a live cell, as if by reproduction.
				if (liveNeighbors == 3) return true;
				return false;
			}
		}
	}

	public static class R12 extends Simulation {
		@Override
		public boolean apply(boolean alive, Grid.Pos pos, Grid grid) {
			Stream<Double> firstNeighbors = grid
					.neighbors(pos)
					.map(p -> grid.isCellAlive(p) ? 1.0 : 0.0);
			
			Stream<Double> secondNeighbors = grid
					.secondNeighbors(pos)
					.map(p -> grid.isCellAlive(p) ? 0.3 : 0.0);
			
			double liveNeighbors = Stream
				.concat(firstNeighbors, secondNeighbors)
				.reduce(0.0, (a,b) -> a + b);
			
				
			if (alive) {
			    // A live cell survives to the next generation if this sum falls within the range of 2.0 - 3.3.
				if (liveNeighbors >= 2.0 && liveNeighbors <= 3.3) return true;
				//  Otherwise it dies (becomes an empty space).
				return false;
			} else {
				// A cell is born into an empty space if this sum falls within the range of 2.3 - 2.9
				if (liveNeighbors >= 2.3 && liveNeighbors <= 2.9) return true;
				return false;
			}
		}
	}
}